import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class InstructionMessageTest {
    
    private InstructionMessage message1;
    private InstructionMessage message2;
    private InstructionMessage message3;
    private InstructionMessage message4;
    private InstructionMessage message5;
    private InstructionQueue queue;
    
    @Test
    public void instructionMessageConstructorAndSettersReturnCorrectly() {
        message1 = new InstructionMessage("InstructionMessage",123123, 456, 200);
    
        assertEquals(message1.getInstructionMessage(), "InstructionMessage");
        assertEquals(message1.getProductCode(), 123123);
        assertEquals(message1.getQuantity(), 456);
        assertEquals(message1.getUom(), 200);
    }
    
    @Test
    public void enqueuePutsInstructionMessageIntoQueue() {
        message1 = new InstructionMessage("InstructionMessage", 123123, 456, 200);
        queue = new InstructionQueue();
        queue.enqueue(message1);
        assertEquals(queue.getFirstElementInQueue(), message1);
    }
    
    @Test
    public void showReturnsEntireQueue() {
        message1 = new InstructionMessage("InstructionMessage", 123123, 456, 200);
        message2 = new InstructionMessage("InstructionMessage", 111111, 222, 200);
        queue = new InstructionQueue();
        queue.enqueue(message1);
        queue.enqueue(message2);
        assertEquals(queue.show(), List.of(message1, message2));
    }
    
    @Test
    public void instructionMessageOnlyQueuedWhenValid() {
        message1 = new InstructionMessage("InstructionMessage", 123456, 1, 200);
        queue = new InstructionQueue();
        queue.enqueue(message1);
        assertEquals(queue.show(), List.of(message1));
    
        assertThrows(IllegalArgumentException.class, () -> {
            message2 = new InstructionMessage("InstructionMessagee", 123456, 1, 200);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            message3 = new InstructionMessage("InstructionMessage", 2445, 1, 200);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            message4 = new InstructionMessage("InstructionMessage", 123456, -1, 200);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            message5 = new InstructionMessage("InstructionMessage", 123456, 1, 298);
        });
    }
    
    @Test
    public void isEmptyReturnsTrueWhenQueueIsEmptyAndFalseWhenQueueIsNotEmpty() {
        message1 = new InstructionMessage("InstructionMessage", 123456, 1, 200);
        queue = new InstructionQueue();
        assertTrue(queue.isEmpty());
        queue.enqueue(message1);
        assertFalse(queue.isEmpty());
    }
    
    @Test
    public void countReturnsThreeWhenThreeMessagesAdded() {
        message1 = new InstructionMessage("InstructionMessage", 123456, 1, 100);
        message2 = new InstructionMessage("InstructionMessage", 112233, 14, 200);
        message3 = new InstructionMessage("InstructionMessage", 102938, 1345, 17);
        queue = new InstructionQueue();
        queue.enqueue(message1);
        queue.enqueue(message2);
        queue.enqueue(message3);
        assertEquals(3, queue.count());
    }
    
}
