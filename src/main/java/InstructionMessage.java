public class InstructionMessage implements MessageReceiver {
    
    private final String INSTRUCTION_MESSAGE = "InstructionMessage";
    private int productCode;
    private int quantity;
    private int uom;
    
    public InstructionMessage(String instructionMessage, int productCode, int quantity, int uom) {
        if (!instructionMessage.equals(INSTRUCTION_MESSAGE))
            throw new IllegalArgumentException("The instruction message must start with " + INSTRUCTION_MESSAGE);
        if (countDigits(productCode) != 6)
            throw new IllegalArgumentException("The product code must be six digits");
        if (quantity <= 0 )
            throw new IllegalArgumentException("The quantity must be greater than 0");
        if (uom < 0 || uom > 255)
            throw new IllegalArgumentException("The UOM must be between 0 and 255 inclusive");
        
        this.productCode = productCode;
        this.quantity = quantity;
        this.uom = uom;
    }
    
    private int countDigits(int num) {
        int count = 0;
        while (num != 0) {
            num /= 10;
            count++;
        }
        return count;
    }
    
    
    @Override
    public void receive(String message) {
    
    }
    
    public String getInstructionMessage() {
        return INSTRUCTION_MESSAGE;
    }
    
    public int getProductCode() {
        return productCode;
    }
    
    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }
    
    public int getQuantity() {
        return quantity;
    }
    
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public int getUom() {
        return uom;
    }
    
    public void setUom(int uom) {
        this.uom = uom;
    }
    
    @Override
    public String toString() {
        return INSTRUCTION_MESSAGE + " " + productCode + " " + quantity + " " + uom;

    }
}
