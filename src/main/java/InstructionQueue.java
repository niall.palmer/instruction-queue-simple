import java.util.ArrayList;
import java.util.List;

public class InstructionQueue {
    
    private final List<InstructionMessage> instructionQueue = new ArrayList<>();
    
    public void enqueue(InstructionMessage message) {
        instructionQueue.add(message);
    }
    
/*
    The old show() method

    public List<String> show() {
        List<String> instructionQueueAsString = new ArrayList<>();
        for (InstructionMessage instructionMessage : instructionQueue) {
            instructionQueueAsString.add(instructionMessage.toString());
        }
        return instructionQueueAsString;
    }
*/
    
    public List<InstructionMessage> show() {
        return instructionQueue;
    }
    
    public InstructionMessage getFirstElementInQueue() {
        return instructionQueue.get(0);
    }
    
    @Override
    public String toString() {
        return super.toString();
    }
    
    public boolean isEmpty() {
        return instructionQueue.isEmpty();
    }
    
    public int count() {
        return instructionQueue.size();
    }
}
