public interface MessageReceiver {
    void receive(String message);
    
}
